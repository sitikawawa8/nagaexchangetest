import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  Image,
  SafeAreaView,
} from 'react-native';
import React, {useEffect, useState} from 'react';

import settingsIcon from '../assets/settingsIcon.png';
import LogoNE from '../assets/NE_LOGO.png';
import logoutIcon from '../assets/logout_icon.png';

export default function List({navigation}) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const RemoveDecimal = digit => {
    return Math.floor(digit);
  };

  const onPressLogout = () => {
    navigation.replace('Login');
  };

  const getVcoinData = async () => {
    try {
      const response = await fetch('https://www.jsonkeeper.com/b/DCQG');
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getVcoinData();
  }, []);

  return (
    <SafeAreaView style={{backgroundColor: '#FFFFFF'}}>
      {/* Cust Header  */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Image
          style={{width: 32, height: 32, margin: 15}}
          source={settingsIcon}
        />
        <Image style={{width: 48, height: 48, margin: 15}} source={LogoNE} />
        <TouchableOpacity onPress={onPressLogout}>
          <Image
            style={{width: 30, height: 30, margin: 15}}
            source={logoutIcon}
          />
        </TouchableOpacity>
      </View>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <View>
          <FlatList
            data={data}
            keyExtractor={({name}) => name}
            renderItem={({item}) => (
              <TouchableOpacity style={styles.touchableView}>
                <View>
                  <Image
                    source={{
                      uri: item.icon,
                    }}
                    style={{width: 32, height: 32}}
                  />
                </View>
                <View style={styles.contentView}>
                  <Text style={{fontSize: 18, color: '#6E7499'}}>
                    {item.symbol} / {item.pair}
                  </Text>
                  <Text style={{fontSize: 20, color: '#605757'}}>
                    {item.pair} {RemoveDecimal(item.price)}
                  </Text>
                  <Text style={{color: '#24A959'}}>Vol {item.vol}</Text>
                </View>
                {item.change < 0 ? (
                  <View
                    style={[styles.badgeView, {backgroundColor: '#F35242'}]}>
                    <Text style={{color: '#FFFFFF'}}>{item.change}</Text>
                  </View>
                ) : (
                  <View
                    style={[styles.badgeView, {backgroundColor: '#24A959'}]}>
                    <Text style={{color: '#FFFFFF'}}>+{item.change}</Text>
                  </View>
                )}
              </TouchableOpacity>
            )}
          />
        </View>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  touchableView: {
    flexDirection: 'row',
    padding: 20,
    backgroundColor: '#F0F0F0',
    borderRadius: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    width: '97%',
    marginTop: 10,
  },
  contentView: {
    right: 30,
  },
  badgeView: {
    borderRadius: 20,
    width: 71,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
