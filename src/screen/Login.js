import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native';
import Button from '../components/Button';
const App = ({navigation}) => {
  const onPressLogin = () => {
    navigation.navigate('List');
  };
  const [state, setState] = useState({
    email: '',
    password: '',
  });
  return (
    <View style={styles.container}>
      <Text style={styles.title}> Login</Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          placeholder="Email"
          placeholderTextColor="#003f5c"
          onChangeText={text => setState({email: text})}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          style={styles.inputText}
          secureTextEntry
          placeholder="Password"
          placeholderTextColor="#003f5c"
          onChangeText={text => setState({password: text})}
        />
      </View>
      <Button
        size={'large'}
        onPress={onPressLogin}
        type="filled"
        bordered
        text={'Masuk Ke Akun'}
      />
      {/* <TouchableOpacity onPress={onPressLogin} style={styles.loginBtn}>
        <Text style={styles.loginText}>Masuk Ke Akun </Text>
      </TouchableOpacity> */}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 24,
    color: '#000000',
    marginBottom: 40,
  },
  inputView: {
    width: '80%',
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderColor: '#000000',
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 20,
  },
  inputText: {
    height: 50,
    color: '#000000',
  },
  forgotAndSignUpText: {
    color: '#000000',
    fontSize: 11,
  },
  loginBtn: {
    width: '80%',
    backgroundColor: '#fb5b5a',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40,
    marginBottom: 10,
  },
});
export default App;
